# Why Gitlab is (much) better than Github

As of August 2021, there are two big troubles for using Github:

1) **Github Markdown doesn't support hierarchical relative links for pages**

For example, for a typical file link, you would write:

```
[Label](Relative link to the file)
```
But what if the file is under a subdirectory ?

Github doesn't support that. 
See : https://github.com/foambubble/foam/blob/master/docs/features/link-reference-definitions.md

2) **Github doesn't support native diagrams like PlantUML or Mermaid. Gitlab can support.**

See Github integration downsides here: https://blog.anoff.io/2018-07-31-diagrams-with-plantuml/

So I decided to move this Repo & WIKI to Gitlab, and I will wait until Github support these to.

### Reference readings:

https://github.com/foambubble/foam/issues/427

https://github.com/foambubble/foam/issues/16

https://github.community/t/feature-request-support-mermaid-markdown-graph-diagrams-in-md-files/1922/183

