# repo-documentation-template

A template repository for "adequate" documentation for opensource projects.

Every repository should include **at least below 4 markdown files** in the top directory:
- [README.md](README)
- [LICENSE.md](LICENSE)
- [CONTRIBUTING.md](CONTRIBUTING)
- [HACKING.md](HACKING)

This is my personal opinion, and please feel free to comment on it. 

**Please see [WHY GITLAB](Why-Gitlab.md)**

README.md starts with **below contents** within seperators. Copy&paste to your README for a good start.

----

## Your Fancy Repository Name

Your Readme.md file usually starts with above title. In this part, you should have a **brief description** of the repository.

Please have a look at https://www.makeareadme.com/

Additionally, **consider having an icon/badge** for your repository upleft.

## Purpose & Description

You should describe the purpose of this software here. 

Please do not make any assumptions on the experience or knowledge of the reader ! Use simple english and welcoming attitude.

Is this a library ? A standalone cli application ? A go module ? Where is it used ? Provide some useful links for a pre-studying.

## Target Audience ( Optional )

This part depends on the type of the software / package. Feel free to omit it, however it would be nice to have it, if it is relevant.

## Target Platforms / Devices / Hardware ( Optional )

This part is important. Does this software runs on only amd64 ? Or armhf ? How about Risc-V ? 

Or inside a microk8s cluster ?

## Building & Installation

### Prerequisites

You should definitely describe prerequisites here ! Please **do not make any assumptions on the reader's environment** ! 

Please define what are required to build & run this software as below:

- An ubuntu 20.04 amd64 desktop machine
- LXD must be setup and configured in auto mode as decribed [here]()
- You need a USB TTL cable to capture startup logs
- sudo apt install libfoo0.1-dev libbar-dev
- etc

### Building

Rough example:

```bash
$ git clone this repo
$ cd this repo
$ make -j8
```

### Installation

Rough example:

```bash
$ make install
```

## Usage

```bash
$ my-fancy-app

$ #returns foo-bar
```
It would be extremely helpful if you provide some screenshots here. It will be even better if you provide animatedgifs, such as (from [godu](https://github.com/viktomas/godu) project):

<img src="https://user-images.githubusercontent.com/1079718/54609718-cf2bb580-4a53-11e9-8cd0-2272278597b6.gif" width="100%" />

Or, another example from [nnn](https://github.com/jarun/nnn) project:

<p align="center"><a href="http://i.imgur.com/kOld6HT.gif"><img src="https://i.imgur.com/NUsSA2u.jpg"></a></p>

I like how nnn project maintains their documentation and wiki.

## Testing

Describe how to test 

## Architecture and Code structure

Describe code structure, external dependencies...etc

```tree
├── arch  ---> CPU architecture dependent code
├── asserts ---> various bla bla
├── boot  ---> common boot related code
├── bootloader ---> bootloader specific code
├── daemon ---> foobar dameon common code
├── data ---> some generic data for bla bla
├── dbusutil ---> dbus utilities for blabla
├── debian -> packaging/ubuntu-16.04/ ---> in order to create a deb package
├── desktop  ---> desktop shortcuts
├── dirs ---> directories created during bootstrap
├── docs  ---> documentation
```

You may create some diagrams here like (gitlab supports both PlantUML and mermaid out-of-the-box):

```plantuml
@startuml
class BaseClass

namespace net.dummy #DDDDDD {
    .BaseClass <|-- Person
    Meeting o-- Person

    .BaseClass <|- Meeting
}

namespace net.foo {
  net.dummy.Person  <|- Person
  .BaseClass <|-- Person

  net.dummy.Meeting o-- Person
}

BaseClass <|-- net.unused.Person
@enduml
```

## Contributing

See [Contributing](CONTRIBUTING)

## License

See [License](LICENSE)

----
## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, **too long is better than too short**. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

### Visuals
Depending on what you are making, it can be a good idea to **include screenshots or even a video** (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

### Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

### Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

### Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

### Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

### Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

